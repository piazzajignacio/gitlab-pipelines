terraform {

    required_providers {
      
      aws = {
        source = "hashicorp/aws"
        version = "~> 4.0"
      }
    }
}

provider "aws" {
    region = "us-east-1"
    
}


data "aws_ami" "latest_aws_linux_image"{
    most_recent = true
    owners =  ["amazon"] 
    filter {
        name = "name"
        values = ["amzn2-ami-kernel-*-x86_64-gp2"]
    }
}

resource "random_pet" "instance" {
  
}


resource "aws_vpc" "gitlabvpc" {
  cidr_block = "172.16.0.0/16"
  enable_dns_hostnames = true
  enable_dns_support = true

  tags = {
    Name = "vpc_gitlab-runner"
  }
}


resource "aws_subnet" "runner-subnet" {
    vpc_id = aws_vpc.gitlabvpc.id
    cidr_block = "172.16.10.0/24"
    availability_zone = "us-east-1a"
    map_public_ip_on_launch = true

    tags = {
      Name = "Gitlab-runner-subnet"
    }
  
}
resource "aws_instance" "gitlab-runner-serv" {
  count                       = "${var.ec2_count}"
  ami                         = "${data.aws_ami.latest_aws_linux_image.id}"
  instance_type               = "${var.instance_type}"
  subnet_id                   = aws_subnet.runner-subnet.id
  security_groups = ["${aws_security_group.instance-sg.id}"]
  associate_public_ip_address = true
  key_name = "${aws_key_pair.ssh_key_runner.id}"
  user_data = "${file("userdata.sh")}"

  tags = {
    "Name" = "${lookup(var.map_of_type_instances, var.type_instance)}"
  }
}

resource "aws_security_group" "instance-sg" {
  name = "${var.type_instance}+${random_pet.instance.id}"
  description = "security group for gitlab-runner"
  vpc_id = aws_vpc.gitlabvpc.id
  ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]

  }
  egress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}


## we define a internet gateway

resource "aws_internet_gateway" "gitlabrunner-ig" {
  vpc_id = aws_vpc.gitlabvpc.id
  
}

resource "aws_route_table" "gitlab-runner-rt" {
  vpc_id = aws_vpc.gitlabvpc.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gitlabrunner-ig.id

  }
  
}

resource "aws_route_table_association" "gitlab-runner-rta" {
  subnet_id = aws_subnet.runner-subnet.id
  route_table_id = aws_route_table.gitlab-runner-rt.id
  
}


