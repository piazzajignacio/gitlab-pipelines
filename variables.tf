variable "ec2_count" {
  default = "1"
}


variable "instance_type" {
  default = "t2.micro"
}


variable "type_instance" {
  type = string
  default = "server-runner"
  
}
variable "map_of_type_instances" {
    type = map(string)

    default = {
      servertest = "server-test"
      serverprod = "server-prod"
      server-jump = "server-jump"
      server-runner = "gitlab-runner-serv"
}
}

