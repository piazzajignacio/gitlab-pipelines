#!/bin/bash

export PATH=$PATH:usr/local/bin
sudo -i
#Update dependencies:
sudo yum -y update

#Install Git:
sudo yum install -y git

#Install Docker:
sudo amazon-linux-extras install -y docker

#Add the ec2-user user to the docker group:
sudo usermod -aG docker ec2-user

#Ensure the Docker daemon starts automatically:
sudo systemctl enable docker

#Restart your EC2 instance:
sudo reboot

#Add the official GitLab repository:
curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.rpm.sh" | sudo bash
#Install the GitLab Runner:
sudo yum install gitlab-runner -y

#Register the GitLab Runner:
sudo gitlab-runner register

Edit the config.toml file:
sudo vim /etc/gitlab-runner/config.toml

The GitLab CI pipeline config used to test the runner:
https://gitlab.com/-/snippets/2300614